require 'json'
require 'yaml'
require 'open-uri'

include FileUtils

# Jekyll comand to generate markdown collection pages from CSV/YML/JSON records
class FetchPosts < Jekyll::Command
  class << self
    def init_with_program(prog)
      prog.command(:fetchposts) do |c|
        c.syntax 'fetchposts'
        c.description 'Generate md pages from collection data.'
        c.action { || execute() }
      end
    end

    def execute()
      config = YAML.load_file('_config.yml')
      abort 'Cannot find fetch_posts in config' unless config.key?('fetch_posts')
      config = config['fetch_posts']
      if !config.kind_of?(Array)
        config = [config]
      end
      config.each do |d|
        begin
          source = JSON.load(open(d['json']))
          data_source = '_data'
          path = "#{data_source}/#{d['data']}.json"
          open(path, 'wb') do |file|
            file << JSON.generate(source)
          end
          puts "generated #{path}"
        rescue StandardError
          raise ' an error has occurred.'
        end
      end
    end
  end
end
