Gem::Specification.new do |s|
  s.name          = 'fetchposts'
  s.version       = '1.0.0'
  s.date          = '2019-02-08'
  s.summary       = 'jekyll fetchposts plugin'
  s.description   = 'jekyll plugin for retrieving json docs'
  s.authors       = ['Massimo Mori']
  s.files         = ['lib/fetchposts.rb']
  s.require_path  = 'lib'
  s.homepage      = 'https://monema.it'
  s.license       = 'MIT'

  s.add_dependency 'jekyll', '~> 3.8'

  s.add_development_dependency 'bundler', '~> 1.16'
  s.add_development_dependency 'rspec', '~> 3.8'
  s.add_development_dependency 'rubocop', '>= 0.5'
end
